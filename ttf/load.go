package ttf

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"math"

	"github.com/ConradIrwin/font/sfnt"
	"github.com/golang/freetype/truetype"
	"golang.org/x/image/font"
	"golang.org/x/image/math/fixed"
)

type Fixed struct {
	I int16 // integer part
	F int16 // fractional part
}

func (f Fixed) Ceil() int {
	d := float64(f.I) + float64(f.F)/65536
	return int(math.Ceil(d))
}

func (f Fixed) Float32() float32 {
	return float32(f.I) + float32(f.F)/65536
}

func (f Fixed) Int52_12() fixed.Int52_12 {
	return fixed.Int52_12(int64(f.I)<<12 + int64(f.F)>>20)
}

/*type File struct {
	Header         *Header
	HorizMetrics   *HorizMetrics
	PostScriptData *PostScriptData
}
*/
type PostScriptData struct {
	Format             Fixed
	ItalicAngle        Fixed
	UnderlinePosition  int16 // in font design units
	UnderlineThickness int16 // in font design units
	IsFixedPitch       bool
	Names              []string
}

func parse(src []byte) (*FontMetrics, error) {

	// this is stupid... we are using two different truetype
	// parsers, PLUS doing some parsing ourself that is REDUNDANT
	// with what some of them are doing!  Clearly either there is
	// a better library out there, or we are missing a properly
	// abstracted library.  Each one only does what it needs, none
	// of which aligns with our needs.  The one buried in
	// github.com/jung-kurt/gofpdf is closest, but also overly
	// tied with that library which does not do what I want
	// either.
	tfnt, err := truetype.Parse(src)
	if err != nil {
		return nil, err
	}

	b := bytes.NewReader(src)
	fnt, err := sfnt.Parse(b)
	if err != nil {
		return nil, err
	}

	h, err := loadHeader(fnt)
	if err != nil {
		return nil, err
	}

	/*hhdr, err := loadHorizHeader(fnt)
	if err != nil {
		return nil, err
	}*/

	num, err := loadNumGlyphs(fnt)
	if err != nil {
		return nil, err
	}

	post, err := loadPostscriptData(fnt)
	if err != nil {
		return nil, err
	}

	os2, err := loadOS2(fnt)
	if err != nil {
		return nil, err
	}

	//fmt.Printf("FIRST %d LAST %d\n", os2.USFirstCharIndex, os2.USLastCharIndex)
	//fmt.Printf("weight class %d\n", os2.USWeightClass)

	scaled := func(l int16) int {
		return int(math.Round(float64(l) * 1000 / float64(h.UnitsPerEm)))
	}
	bb := tfnt.Bounds(fixed.I(1000))

	fm := &FontMetrics{
		Name:        tfnt.Name(truetype.NameIDPostscriptName),
		Chars:       make([]charMetrics, num),
		Ascent:      scaled(os2.STypoAscender),
		Descent:     scaled(os2.STypoDescender),
		Flags:       []string{},
		ItalicAngle: post.ItalicAngle.Float32(),
		Leading:     int(os2.STypoLineGap),
		CapHeight:   int(os2.SCapHeight),
		XHeight:     int(os2.SXHeight),
		FontWeight:  int(os2.USWeightClass),
		FontBBox: []int{
			bb.Min.X.Floor(),
			bb.Min.Y.Floor(),
			bb.Max.X.Ceil(),
			bb.Max.Y.Ceil(),
		},
		//Encoding: "",
	}

	bold := (h.MacStyle & (1 << 0)) != 0
	italic := (h.MacStyle & (1 << 1)) != 0

	// it might not be set in the MacStyle, but if there is an
	// italic angle, mark it anyway
	if !italic && post.ItalicAngle.Ceil() != 0 {
		italic = true
	}

	// this choice of StemV is a little janky, but not sure
	// exactly what it matters.  Values taken from
	// github.com/jung-kurt/gofpdf
	if bold {
		fm.StemV = 120
	} else {
		fm.StemV = 70
	}
	// even jankier (in particular, I note that cmunti, an italic font,
	// does not have the italic bit set in the header)
	if italic {
		fm.ItalicAngle = -20
		fm.Flags = append(fm.Flags, FontFlagItalic)
	}
	// must mention either ffSymbolic or ffNonsymbolic, and not both
	// note that ffSymbolic just means that there are glyphs outside
	// of the Adobe Latin encoding, which will usually be the case nowadays
	fm.Flags = append(fm.Flags, FontFlagSymbolic)

	// how do we figure this out?
	fm.Flags = append(fm.Flags, FontFlagSerif)

	var gbuf truetype.GlyphBuf
	for index := range fm.Chars {
		gbuf.Load(tfnt, fixed.I(1000), truetype.Index(index), font.HintingNone)
		var name string
		if index < len(post.Names) {
			name = post.Names[index]
		} else {
			name = fmt.Sprintf("index%d", index)
		}
		fm.Chars[index] = charMetrics{
			Name:    name,
			Advance: gbuf.AdvanceWidth.Floor(),
			BBox: []int{
				gbuf.Bounds.Min.X.Floor(),
				gbuf.Bounds.Min.Y.Floor(),
				gbuf.Bounds.Max.X.Ceil(),
				gbuf.Bounds.Max.Y.Ceil(),
			},
		}
	}

	fm.RuneIndex = make(map[string]int)

	var i rune = rune(os2.USFirstCharIndex)
	for i <= rune(os2.USLastCharIndex) {
		index := tfnt.Index(i)
		if index != 0 {
			fm.RuneIndex[runeToGlyph[i]] = int(index)
		}
		i++
	}

	return fm, nil
}

// https://docs.microsoft.com/en-us/typography/opentype/spec/post

type ttfReader struct {
	src []byte
}

func (t *ttfReader) scanUint32() uint32 {
	n := binary.BigEndian.Uint32(t.src)
	t.src = t.src[4:]
	return n
}

// note that we throw away 4 bits of precision because
// the fixed form in TTF is 16.16 but Int52_12 is 52.12
func (t *ttfReader) scanFixed() Fixed {
	i := int16(t.scanUint16())
	f := int16(t.scanUint16())
	return Fixed{
		I: i,
		F: f,
	}
}

func (t *ttfReader) scanInt16() int16 {
	n := binary.BigEndian.Uint16(t.src)
	t.src = t.src[2:]
	return int16(n)
}

func (t *ttfReader) scanUint16() uint16 {
	n := binary.BigEndian.Uint16(t.src)
	t.src = t.src[2:]
	return n
}

func (t *ttfReader) scanUint8() uint8 {
	n := t.src[0]
	t.src = t.src[1:]
	return n
}

// scan a pascal string (8 bit length followed by data)
func (t *ttfReader) scanPascal() string {
	n := t.scanUint8()
	s := string(t.src[:n])
	t.src = t.src[n:]
	return s
}

func loadPostscriptData(f *sfnt.Font) (*PostScriptData, error) {
	post := sfnt.MustNamedTag("post")

	p, err := f.Table(post)
	if err != nil {
		return nil, err
	}
	//fmt.Printf("---(%s)---\n", p.Name())

	rd := ttfReader{p.Bytes()}
	d := &PostScriptData{}

	d.Format = rd.scanFixed()
	//fmt.Printf("FORMAT %d\n", d.Format)

	d.ItalicAngle = rd.scanFixed()
	d.UnderlinePosition = rd.scanInt16()
	d.UnderlineThickness = rd.scanInt16()
	d.IsFixedPitch = rd.scanUint32() != 0
	rd.scanUint32() // minMemType42
	rd.scanUint32() // maxMemType42
	rd.scanUint32() // minMemType1
	rd.scanUint32() // maxMemType1

	if d.Format.I == 2 {
		// format 2, read a subtable
		n := rd.scanUint16()
		//fmt.Printf("numGlyphs = %d\n", n)
		namex := make([]uint16, n)
		for i := range namex {
			namex[i] = rd.scanUint16()
		}
		var namelist []string
		for len(rd.src) > 1 {
			namelist = append(namelist, rd.scanPascal())
		}
		d.Names = make([]string, n)
		for i, x := range namex {
			if x < 258 {
				d.Names[i] = macStandardGlyphs[x]
			} else {
				d.Names[i] = namelist[x-258]
			}
		}
	}

	return d, nil
}

type HorizMetrics struct {
	Metrics []HMetric
}

type HMetric struct {
	Advance         uint16
	LeftSideBearing int16
}

func loadHorizontalMetrics(f *sfnt.Font, hdr *HHeader) (*HorizMetrics, error) {
	p, err := f.Table(sfnt.TagHmtx)
	if err != nil {
		return nil, err
	}
	//fmt.Printf("---(%s)---\n", p.Name())

	rd := ttfReader{p.Bytes()}

	n := hdr.NumOfLongHorMetrics
	hm := make([]HMetric, n)
	for i := range hm {
		hm[i].Advance = rd.scanUint16()
		hm[i].LeftSideBearing = rd.scanInt16()
	}

	return &HorizMetrics{Metrics: hm}, nil
}

type HHeader struct {
	Version             Fixed
	Ascent              int16
	Descent             int16
	LineGap             int16
	AdvanceWidthMax     uint16
	MinLeftSideBearing  int16
	MinRightSideBearing int16
	XMaxExtent          int16
	CaretSlopeRise      int16
	CaretSlopeRun       int16
	CaretOffset         int16
	Reserved1           int16
	Reserved2           int16
	Reserved3           int16
	Reserved4           int16
	MetricDataformat    int16
	NumOfLongHorMetrics int16
}

// annoyingly, this data is already being parsed by the library, but
// it is private
func loadHorizHeader(f *sfnt.Font) (*HHeader, error) {
	p, err := f.Table(sfnt.TagHhea)
	if err != nil {
		return nil, err
	}
	//fmt.Printf("---(%s)---\n", p.Name())

	rd := ttfReader{p.Bytes()}
	h := &HHeader{}

	h.Version = rd.scanFixed()
	h.Ascent = rd.scanInt16()
	h.Descent = rd.scanInt16()
	h.LineGap = rd.scanInt16()
	h.AdvanceWidthMax = rd.scanUint16()
	h.MinLeftSideBearing = rd.scanInt16()
	h.MinRightSideBearing = rd.scanInt16()
	h.XMaxExtent = rd.scanInt16()
	h.CaretSlopeRise = rd.scanInt16()
	h.CaretSlopeRun = rd.scanInt16()
	h.CaretOffset = rd.scanInt16()
	rd.scanInt16() // reserved
	rd.scanInt16() // reserved
	rd.scanInt16() // reserved
	rd.scanInt16() // reserved
	h.MetricDataformat = rd.scanInt16()
	h.NumOfLongHorMetrics = rd.scanInt16()
	return h, nil
}

func loadNumGlyphs(f *sfnt.Font) (uint16, error) {
	p, err := f.Table(sfnt.TagMaxp)
	if err != nil {
		return 0, err
	}
	rd := ttfReader{p.Bytes()}
	rd.scanFixed()
	return rd.scanUint16(), nil
}

type Header struct {
	MajorVersion       uint16
	MinorVersion       uint16
	FontRevision       Fixed
	ChecksumAdjustment uint32
	Magic              uint32
	Flags              uint16
	UnitsPerEm         uint16
	CreatedTime        uint32
	ModifiedTime       uint32
	BBoxXMin           int16
	BBoxYMin           int16
	BBoxXMax           int16
	BBoxYMax           int16
	MacStyle           uint16
	LowestRecPPEM      uint16
	FontDirectionHint  int16
}

func loadHeader(f *sfnt.Font) (*Header, error) {
	p, err := f.Table(sfnt.TagHead)
	if err != nil {
		return nil, err
	}
	// for some reason the TagHead table is including the "head" tag itself,
	// so skip that
	rd := ttfReader{p.Bytes()[4:]}

	h := &Header{}
	h.MajorVersion = rd.scanUint16()
	h.MinorVersion = rd.scanUint16()
	h.FontRevision = rd.scanFixed()
	h.ChecksumAdjustment = rd.scanUint32()
	h.Magic = rd.scanUint32()
	h.Flags = rd.scanUint16()
	h.UnitsPerEm = rd.scanUint16()
	h.CreatedTime = rd.scanUint32()
	h.ModifiedTime = rd.scanUint32()
	h.BBoxXMin = rd.scanInt16()
	h.BBoxYMin = rd.scanInt16()
	h.BBoxXMax = rd.scanInt16()
	h.BBoxYMax = rd.scanInt16()
	h.MacStyle = rd.scanUint16()
	h.LowestRecPPEM = rd.scanUint16()
	h.FontDirectionHint = rd.scanInt16()

	//fmt.Printf("%#v\n", h)
	return h, nil
}
