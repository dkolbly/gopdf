package ttf

import (
	"github.com/ConradIrwin/font/sfnt"
)

type Point16 struct {
	X int16
	Y int16
}

func (t *ttfReader) scanPoint16() Point16 {
	x := t.scanInt16()
	y := t.scanInt16()
	return Point16{x, y}
}

type OS2 struct {
	Version                 uint16
	XAvgCharWidth           int16
	USWeightClass           uint16
	USWidthClass            uint16
	FSType                  uint16
	YSubscriptSize          Point16
	YSubscriptOffset        Point16
	YSupersriptSize         Point16
	YSupersriptOffset       Point16
	YStrikeoutSize          int16
	YStrikeoutPosition      int16
	SFamilyClass            int16
	PANOSE                  [10]uint8
	ULUnicodeRange          [4]uint32
	AchVendID               [4]byte
	FSSelection             uint16
	USFirstCharIndex        uint16
	USLastCharIndex         uint16
	STypoAscender           int16
	STypoDescender          int16
	STypoLineGap            int16
	USWinAscent             uint16
	USWinDescent            uint16
	ULCodePageRange         [2]uint32
	SXHeight                int16
	SCapHeight              int16
	USDefaultChar           uint16
	USBreakChar             uint16
	USMaxContext            uint16
	USLowerOpticalPointSize uint16
	USUpperOpticalPointSize uint16
}

func loadOS2(f *sfnt.Font) (*OS2, error) {
	p, err := f.Table(sfnt.TagOS2)
	if err != nil {
		return nil, err
	}
	rd := ttfReader{p.Bytes()}

	t := &OS2{
		Version: rd.scanUint16(),
	}

	v := int(t.Version)
	// TODO, check to see if the length is too short for a V1
	// table, in which case it is a v0 table (see note at
	// https://docs.microsoft.com/en-us/typography/opentype/spec/os2:
	//
	// > Note: Documentation for OS/2 version 0 in Apple’s
	// > TrueType Reference Manual stops at the usLastCharIndex
	// > field and does not include the last five fields of the
	// > table as it was defined by Microsoft. Some legacy TrueType
	// > fonts may have been built with a shortened version 0 OS/2
	// > table. Applications should check the table length for a
	// > version 0 OS/2 table before reading these fields.

	t.XAvgCharWidth = rd.scanInt16()
	t.USWeightClass = rd.scanUint16()
	t.USWidthClass = rd.scanUint16()
	t.FSType = rd.scanUint16()
	t.YSubscriptSize = rd.scanPoint16()
	t.YSubscriptOffset = rd.scanPoint16()
	t.YSupersriptSize = rd.scanPoint16()
	t.YSupersriptOffset = rd.scanPoint16()
	t.YStrikeoutSize = rd.scanInt16()
	t.YStrikeoutPosition = rd.scanInt16()
	t.SFamilyClass = rd.scanInt16()
	for i := 0; i < 10; i++ {
		t.PANOSE[i] = rd.scanUint8()
	}

	for i := 0; i < 4; i++ {
		t.ULUnicodeRange[i] = rd.scanUint32()
	}
	for i := 0; i < 4; i++ {
		t.AchVendID[i] = rd.scanUint8()
	}
	t.FSSelection = rd.scanUint16()
	t.USFirstCharIndex = rd.scanUint16()
	t.USLastCharIndex = rd.scanUint16()

	if v >= 0 {
		// a true version 0 table
		t.STypoAscender = rd.scanInt16()
		t.STypoDescender = rd.scanInt16()
		t.STypoLineGap = rd.scanInt16()
		t.USWinAscent = rd.scanUint16()
		t.USWinDescent = rd.scanUint16()
	}
	if v >= 1 {
		for i := 0; i < 2; i++ {
			t.ULCodePageRange[i] = rd.scanUint32()
		}
	}
	if v >= 4 {
		t.SXHeight = rd.scanInt16()
		t.SCapHeight = rd.scanInt16()
		t.USDefaultChar = rd.scanUint16()
		t.USBreakChar = rd.scanUint16()
		t.USMaxContext = rd.scanUint16()

	}
	if v >= 5 {
		t.USLowerOpticalPointSize = rd.scanUint16()
		t.USUpperOpticalPointSize = rd.scanUint16()
	}
	return t, nil
}
