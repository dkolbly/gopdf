package ttf

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"

	"bitbucket.org/dkolbly/gopdf"
	"github.com/golang/freetype/truetype"
	"golang.org/x/image/math/fixed"
)

const (
	FontFlagFixedPitch  = "FixedPitch"
	FontFlagSerif       = "Serif"
	FontFlagSymbolic    = "Symbolic"
	FontFlagScript      = "Script"
	FontFlagNonsymbolic = "Nonsymbolic"
	FontFlagItalic      = "Italic"
	FontFlagAllCap      = "AllCap"
	FontFlagSmallCap    = "SmallCap"
	FontFlagForceBold   = "ForceBold"
)

type FontMetrics struct {
	Name        string
	Family      string `json:",omitempty"`
	Flags       []string
	FontBBox    []int
	Ascent      int
	Descent     int
	Leading     int
	CapHeight   int
	FontWeight  int `json:",omitempty"`
	ItalicAngle float32
	XHeight     int `json:",omitempty"`
	StemV       int
	StemH       int           `json:",omitempty"`
	Encoding    string        `json:",omitempty"`
	Chars       []charMetrics `json:",omitempty"`
	RuneIndex   map[string]int
}

type charMetrics struct {
	Name    string `json:"name"` // PostScript name of this char
	Advance int
	BBox    []int `json:",omitempty"`
}

type TrueTypeFont struct {
	Name  string
	data  []byte
	enc   []byte
	embed bool

	/*
		fnt            *truetype.Font
		postScriptName gopdf.Name
		embed          bool
		widths         []gopdf.Datum
	*/
	widths         gopdf.Array
	fontDescriptor gopdf.Dict
	// other properties that go in the /Font dict
	fprops  map[string]gopdf.Datum
	metrics *FontMetrics
}

const (
	ffFixedPitch = 1 << iota
	ffSerif
	ffSymbolic
	ffScript
	ffNonsymbolic = 1<<6 + iota
	ffItalic
	ffAllCap = 1<<17 + iota
	ffSmallCap
	ffForceBold
)

// MakeFontDict implements gopdf.Font.  It is responsible for
// installing this font object into the PDF document.  If `subset` is
// present, it indicates which runes of the font have been used in the
// document and hence can be used to subset the font if desired.
func (tt *TrueTypeFont) MakeFontDict(doc *gopdf.Document, subset map[rune]bool) (gopdf.Dict, error) {
	fd := gopdf.NewDict("Font")

	//fd.contents["Subtype"] = name("Type1")
	//fd.contents["BaseFont"] = name("Helvetica")

	//scale := 1000 / float64(tt.fnt.FUnitsPerEm())

	/*	tofixed := func(f float64) fixed.Int26_6 {
			return fixed.Int26_6(f * 64) // 64 => 6 bits of fixed precision
		}
	*/
	for k, v := range tt.fprops {
		fd.Set(k, v)
	}

	fd.Set("Widths", doc.Store(tt.widths))

	// c.f. 9.8 Font Descriptors

	/*

	 desc.contents["Ascent"] = ...
	 desc.contents["Descent"] = ...
	 desc.contents["Leading"] = ...
	 desc.contents["CapHeight"] = ...
	*/
	// desc.contents["XHeight"] = ...
	/*	 desc.contents["StemV"] = ...	*/
	//desc.contents["StemH"] = ...

	//

	if tt.embed {
		s := doc.Stream()
		// for some reason I can't quite discern, I need to use
		// FontFile2 to reference TTF fonts.  FontFile3 doesn't
		// seem to work.  Is it because OpenType and TrueType are
		// slightly different??  :/
		//
		// If I do the FontFile3 thing (and specify the
		// /Subtype here), then evince renders it
		// fine, and pdftoppm *works* but complains with a warning:
		//
		//      Syntax Warning: Mismatch between font type and
		//      embedded font file
		//
		//s.Set("Subtype", gopdf.Name("OpenType"))

		// there is some amiguity as to whether /Length1 is required;
		// it doesn't seem to hurt, and pdf-online.com validation
		// mentions it is missing and Adobe Acrobat can't decode
		// the font without it
		s.Set("Length1", gopdf.Integer(len(tt.data)))
		s.Set("Filter", gopdf.Name("ASCII85Decode"))
		s.Write(tt.enc)

		// copy the descriptor and add the stream reference
		desc := gopdf.NewDict("")

		for k, v := range tt.fontDescriptor.Contents() {
			desc.Set(k, v)
		}
		desc.Set("FontFile2", s.Ref)
		fd.Set("FontDescriptor", doc.Store(desc))
	} else {
		fd.Set("FontDescriptor", doc.Store(tt.fontDescriptor))
	}
	return fd, nil
}

func Load(data []byte) (*TrueTypeFont, error) {
	fnt, err := truetype.Parse(data)
	if err != nil {
		return nil, err
	}

	fm, err := parse(data)
	if err != nil {
		return nil, err
	}

	// c.f. https://developer.apple.com/fonts/TrueType-Reference-Manual/RM06/Chap6name.html
	psn := fnt.Name(truetype.NameIDPostscriptName)

	if psn == "" {
		psn = fnt.Name(truetype.NameIDFontFullName) // full name
		if psn == "" {
			psn = fnt.Name(truetype.NameIDFontFamily)     // font family
			sub := fnt.Name(truetype.NameIDFontSubfamily) // font subfamily
			if sub != "" {
				psn = psn + "-" + sub
			}
		}
	}
	if psn == "" {
		return nil, fmt.Errorf("could not discern a name")
	}
	// sometimes the PS name coming straight out of the font is not
	// a valid PostScript name :/
	psn = psinvalid.ReplaceAllString(psn, "")

	first := 32
	last := 255

	fd := make(map[string]gopdf.Datum)

	psName := gopdf.Name(psn)

	fd["Subtype"] = gopdf.Name("TrueType")
	fd["BaseFont"] = psName
	fd["FirstChar"] = gopdf.Integer(first)
	fd["LastChar"] = gopdf.Integer(last)
	fd["Encoding"] = gopdf.Name("MacRomanEncoding")

	// the missing width is the width of the glyph at index 0
	miss := gopdf.Integer(fm.Chars[0].Advance)

	w := make(gopdf.Array, last-first+1)
	for i := first; i <= last; i++ {
		g := runeToGlyph[rune(i)]
		if index, ok := fm.RuneIndex[g]; ok {
			cm := fm.Chars[index]
			w[i-first] = gopdf.Integer(cm.Advance)
		} else {
			w[i-first] = miss
		}
	}

	// c.f. 9.8 Font Descriptors
	desc := gopdf.NewDict("FontDescriptor")
	desc.Set("FontName", psName)

	fam := fnt.Name(truetype.NameIDFontFamily)
	if fam != "" {
		desc.Set("FontFamily", gopdf.Str(fam))
	}

	desc.Set("MissingWidth", miss)

	desc.Set("Ascent", gopdf.Integer(fm.Ascent))
	desc.Set("Descent", gopdf.Integer(fm.Descent))
	desc.Set("CapHeight", gopdf.Integer(fm.CapHeight))
	//desc.Set("Flags")

	desc.Set("ItalicAngle", gopdf.Integer(fm.ItalicAngle))
	desc.Set("StemV", gopdf.Integer(fm.StemV))
	if fm.StemH != 0 {
		desc.Set("StemH", gopdf.Integer(fm.StemH))
	}

	if fm.FontWeight != 0 {
		// some ttf fonts have invalid weights; Calligrapher has
		// a weight of 5 for example, which is invalid.  Not sure
		// if that' supposed to be 500 :shrug:
		if fm.FontWeight >= 100 {
			desc.Set("FontWeight", gopdf.Integer(fm.FontWeight))
		}
	}

	// c.f. 9.8.2 Font Descriptor Flags
	flags := 0

	for _, f := range fm.Flags {
		switch f {
		case FontFlagFixedPitch:
			flags |= ffFixedPitch
		case FontFlagSerif:
			flags |= ffSerif
		case FontFlagSymbolic:
			flags |= ffSymbolic
		case FontFlagScript:
			flags |= ffScript
		case FontFlagNonsymbolic:
			flags |= ffNonsymbolic
		case FontFlagItalic:
			flags |= ffItalic
		case FontFlagAllCap:
			flags |= ffAllCap
		case FontFlagSmallCap:
			flags |= ffSmallCap
		case FontFlagForceBold:
			flags |= ffForceBold
		}
	}

	desc.Set("Flags", gopdf.Integer(flags))
	bb := fnt.Bounds(fixed.I(1000))

	desc.Set("FontBBox", gopdf.Rect{
		LL: gopdf.Point{
			X: float64(bb.Min.X.Floor()),
			Y: float64(bb.Min.Y.Floor()),
		},
		UR: gopdf.Point{
			X: float64(bb.Max.X.Ceil()),
			Y: float64(bb.Max.Y.Ceil()),
		},
	})

	return &TrueTypeFont{
		Name:           psn,
		fontDescriptor: desc,
		data:           data,
		enc:            gopdf.ASCII85Encode(data),
		fprops:         fd,
		/*
			fnt:            fnt,
			postScriptName: gopdf.Name(psName),
		*/
		embed:   true, // TODO implement licensing constraints (c.f. OS2 fsType)
		widths:  w,
		metrics: fm,
	}, nil
}

var psinvalid = regexp.MustCompile(`[^A-Za-z0-9_-]+`)

var runeToGlyph = make(map[rune]string, 5000)
var glyphToRune = make(map[string]rune, 5000)

func init() {
	// parse the glyph list into two tables
	src := AdobeGlyphList
	for len(src) > 0 {
		semi := strings.IndexByte(src, ';')
		eol := strings.IndexByte(src, '\n')
		line := src

		if eol > 0 {
			line = line[:eol]
			src = src[eol+1:]
		} else {
			src = ""
		}
		if semi > 0 {
			name := line[:semi]
			uni, err := strconv.ParseUint(line[semi+1:], 16, 16)
			if err == nil {
				runeToGlyph[rune(uni)] = name
				glyphToRune[name] = rune(uni)
			}
		}
	}
}

/******************************************************************

                   EXAMPLE FONT EMBEDDING

6 0 obj
<<
  /Type /Font
  /BaseFont /CalligrapherRegular
  /Encoding /WinAnsiEncoding
  /FirstChar 32
  /FontDescriptor 8 0 R
  /LastChar 255
  /Subtype /TrueType
  /Widths 7 0 R
>>
endobj

7 0 obj
[282 324 405 584 632 980 776 259 299 299 377 600 259 432 254
597 529 298 451 359 525 423 464 417 457 479 275 282 600 600 600 501
800 743 636 598 712 608 562 680 756 308 314 676 552 1041 817 729 569
698 674 618 673 805 753 1238 716 754 599 315 463 315 600 547 278 581
564 440 571 450 347 628 611 283 283 560 252 976 595 508 549 540 395
441 307 614 556 915 559 597 452 315 222 315 600 800 800 800 0 0 0 780
0 0 278 0 0 0 1064 800 0 800 800 259 259 470 470 500 300 600 278 990 0
0 790 800 800 754 282 324 450 640 518 603 0 519 254 800 349 0 0 432
800 278 0 0 0 0 278 614 0 254 278 0 305 0 0 0 0 501 743 743 743 743
743 743 1060 598 608 608 608 608 308 308 308 308 0 817 729 729 729 729
729 0 729 805 805 805 805 0 0 688 581 581 581 581 581 581 792 440 450
450 450 450 283 283 283 283 0 595 508 508 508 508 508 0 508 614 614
614 614 0 0 597]
endobj

8 0 obj
<<
  /Type /FontDescriptor
  /Ascent 899
  /CapHeight 899
  /Descent -234
  /Flags 32
  /FontBBox [-173 -234 1328 899]
  /FontFile2 5 0 R
  /FontName /CalligrapherRegular
  /ItalicAngle 0
  /MissingWidth 800
  /StemV 70
>>
endobj

*******************************************************************/
