package ttf

import (
	"golang.org/x/image/math/fixed"
)

type CharMetrics struct {
	Advance fixed.Int26_6
	BBox    fixed.Rectangle26_6
}

type Scaled struct {
	font   *TrueTypeFont
	factor fixed.Int26_6
}

func (t *TrueTypeFont) Scale(scale fixed.Int26_6) *Scaled {
	return &Scaled{
		font:   t,
		factor: scale,
	}
}

func (s *Scaled) scale(x int) fixed.Int26_6 {
	return fixed.Int26_6(int(fixed.I(x).Mul(s.factor)) / 1000)
}

func (s *Scaled) Metrics(r rune) *CharMetrics {
	g, ok := runeToGlyph[r]
	if !ok {
		// can't figure out the glyph name
		return nil
	}
	// find the glyph index in the font
	k, ok := s.font.metrics.RuneIndex[g]
	if !ok {
		// there is no such glyph in the font
		return nil
	}
	unscaled := s.font.metrics.Chars[k]
	ch := &CharMetrics{
		Advance: s.scale(unscaled.Advance),
		BBox: fixed.Rectangle26_6{
			Min: fixed.Point26_6{
				X: s.scale(unscaled.BBox[0]),
				Y: s.scale(unscaled.BBox[1]),
			},
			Max: fixed.Point26_6{
				X: s.scale(unscaled.BBox[2]),
				Y: s.scale(unscaled.BBox[3]),
			},
		},
	}
	return ch
}
