package gopdf

import (
	"fmt"
	"io"
	"sort"
)

// a datum is a "value" in PDF
type Datum interface {
	WriteTo(io.Writer) error
}

type Array []Datum

func (a Array) WriteTo(dst io.Writer) error {
	lb := newLineBreaker(dst)
	fmt.Fprintf(lb, "[")
	for i, item := range a {
		if i > 0 {
			lb.space(72)
		}
		item.WriteTo(lb)
	}
	fmt.Fprintf(lb, "]")
	return nil
}

type Binary []byte

func (h Binary) WriteTo(dst io.Writer) error {
	fmt.Fprintf(dst, "<")
	for i := 0; i < len(h); i++ {
		if i > 0 && i%64 == 0 {
			dst.Write([]byte{'\n'})
		}
		fmt.Fprintf(dst, "%02x")
	}
	fmt.Fprintf(dst, ">")
	return nil
}

type Str string

func (s Str) WriteTo(dst io.Writer) error {
	// TODO escape (c.f. 7.3.4.2)
	_, err := fmt.Fprintf(dst, "(%s)", string(s))
	return err
}

type Name string

func (s Name) WriteTo(dst io.Writer) error {
	// TODO escape (c.f. 7.3.5)
	_, err := fmt.Fprintf(dst, "/%s", string(s))
	return err
}

type Integer int64

func (i Integer) WriteTo(dst io.Writer) error {
	_, err := fmt.Fprintf(dst, "%d", i)
	return err
}

type objref struct {
	id  uint
	gen uint16
}

func (o objref) WriteTo(dst io.Writer) error {
	_, err := fmt.Fprintf(dst, "%d %d R", o.id, o.gen)
	return err
}

type Dict struct {
	// PDF dict keys are always "names", we shorten that to strings here
	contents map[string]Datum
}

func NewDict(n Name) Dict {
	d := Dict{
		contents: make(map[string]Datum),
	}
	if n != "" {
		d.contents["Type"] = n
	}
	return d
}

func (d Dict) Set(k string, v Datum) {
	d.contents[k] = v
}

func (d Dict) Contents() map[string]Datum {
	return d.contents
}

func (d Dict) WriteTo(dst io.Writer) error {
	var keys sort.StringSlice
	for k := range d.contents {
		keys = append(keys, k)
	}
	sort.Sort(keys)

	fmt.Fprintf(dst, "<< ")
	// always write the type first, if present
	first := true
	if t, ok := d.contents["Type"]; ok {
		fmt.Fprintf(dst, "/Type ")
		t.WriteTo(dst)
		first = false
	}

	for _, k := range keys {
		v := d.contents[k]
		if k == "Type" {
			// already wrote the /Type
			continue
		}
		if !first {
			fmt.Fprintf(dst, "\n   ")
		}
		fmt.Fprintf(dst, "/%s ", k)
		v.WriteTo(dst)
		first = false
	}
	fmt.Fprintf(dst, " >>\n")
	return nil
}
