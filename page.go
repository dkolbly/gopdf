package gopdf

// c.f. 7.7.3.3 Page Objects
type Page struct {
	Dict
	owner     *Document
	self      *object
	content   *Stream
	labelmask uint
	label     PageLabel
	usesFonts map[*LoadedFont]bool
}

func (p *Pages) AddPage(media Rect) *Page {
	obj := p.owner.alloc()
	pg := &Page{
		Dict:      NewDict(Name("Page")),
		owner:     p.owner,
		self:      obj,
		usesFonts: make(map[*LoadedFont]bool),
	}
	obj.item = pg
	pg.contents["Parent"] = p.ref()
	pg.contents["Resources"] = NewDict("")
	pg.contents["MediaBox"] = media

	p.kids = append(p.kids, pg)
	return pg
}

func (p *Page) flush() {
	// fix up the fonts dict
	if len(p.usesFonts) > 0 {
		d := NewDict("")
		p.resources().contents["Font"] = d
		for lf := range p.usesFonts {
			d.contents[lf.key] = lf.obj.ref()
		}
	}
}

func (p *Page) walk(v pageVisitor) {
	v.leaf(p)
}

func (p *Page) resources() Dict {
	return p.contents["Resources"].(Dict)
}

func (p *Page) ref() objref {
	return p.self.ref()
}

func (p *Page) count() int {
	return 1
}

func (p *Page) Content() *Stream {
	if p.content == nil {
		s := p.owner.Stream()
		p.content = s
		p.contents["Contents"] = s.Ref
	}
	return p.content
}

func (p *Page) PageLabel(n int) *Page {
	p.label.mask |= 0x1
	p.label.Number = n
	return p
}

func (p *Page) PageLabelPrefix(s string) *Page {
	p.label.mask |= 0x2
	p.label.Prefix = s
	return p
}

func (p *Page) PageLabelStyle(s PageLabelStyle) *Page {
	p.label.mask |= 0x4
	p.label.Style = s
	return p
}

type PageLabelStyle int

const (
	DecimalArabic    = PageLabelStyle(iota) // 1, 2, 3, ...
	UppercaseRoman                          // I, II, III, IV, ...
	LowercaseRoman                          // i, ii, iii, iv, ...
	UppercaseLetters                        // A, ..., Z, AA, ..., ZZ, AAA, ...
	LowercaseLetters                        // a, ..., z, aa, ..., zz, aaa, ...
)

// see 12.4.2 Page Labels

type PageLabel struct {
	Style  PageLabelStyle
	Prefix string
	Number int
	mask   uint
}

func (p PageLabelStyle) tag() Name {
	switch p {
	case DecimalArabic:
		return Name("D")
	case UppercaseRoman:
		return Name("R")
	case LowercaseRoman:
		return Name("r")
	case UppercaseLetters:
		return Name("A")
	case LowercaseLetters:
		return Name("a")
	default:
		panic("invalid page label style")
	}
}
