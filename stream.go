package gopdf

import (
	"bytes"
	"fmt"
	"io"
)

type Stream struct {
	Ref    objref
	header map[string]Datum
	buf    bytes.Buffer
}

func (s *Stream) WriteTo(dst io.Writer) error {
	s.header["Length"] = Integer(s.buf.Len())
	d := &Dict{
		contents: s.header,
	}
	d.WriteTo(dst)
	fmt.Fprintf(dst, "stream\n")
	dst.Write(s.buf.Bytes())
	fmt.Fprintf(dst, "\nendstream\n")
	return nil
}

func (d *Document) Stream() *Stream {
	o := d.alloc()
	s := &Stream{
		Ref: objref{
			id:  o.id,
			gen: o.gen,
		},
		header: make(map[string]Datum),
	}
	o.item = s
	return s
}

func (s *Stream) Write(data []byte) (int, error) {
	return s.buf.Write(data)
}

func (s *Stream) Close() error {
	return nil
}

// Set writes entries into the header dict of the stream
func (s *Stream) Set(k string, v Datum) {
	s.header[k] = v
}
