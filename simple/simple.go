package main

import (
	"io/ioutil"
	"os"
	"time"

	"bitbucket.org/dkolbly/gopdf"
	"bitbucket.org/dkolbly/gopdf/ttf"
	"golang.org/x/image/math/fixed"
)

var cmunti = "/home/donovan/.fonts/Computer-Modern/cmunti.ttf"
var calligra = "/home/donovan/z/at/src/github.com/jung-kurt/gofpdf/font/calligra.ttf"
var crim = "/tmp/crim/CrimsonText-Italic.ttf"

func main() {
	buf, err := ioutil.ReadFile(calligra)
	if err != nil {
		panic(err)
	}
	cal, err := ttf.Load(buf)
	if err != nil {
		panic(err)
	}

	buf, err = ioutil.ReadFile(cmunti)
	if err != nil {
		panic(err)
	}
	cm, err := ttf.Load(buf)
	if err != nil {
		panic(err)
	}

	fonts := []gopdf.Font{
		cal,
		cm,
	}

	d := gopdf.New()
	t := time.Now()

	d.SetInfo("Producer", "gopdf/1.0.0")
	d.SetInfo("CreationDate", t.Format("D:20060102150405"))

	//fmt.Fprintf(d.Stream(), "Hello, world\n")

	pic := gopdf.NewRect(0, 0, 350, 250)

	p1 := d.Pages.AddPage(pic).PageLabelStyle(gopdf.LowercaseRoman)
	pic1(fonts, p1)

	p2 := d.Pages.AddPage(pic)
	pic2(fonts, p2)

	p3 := d.Pages.AddPage(pic).PageLabel(1).PageLabelStyle(gopdf.DecimalArabic)
	pic3(fonts, p3)

	p4 := d.Pages.AddPage(pic)
	pic3(fonts, p4)

	p5 := d.Pages.AddPage(pic).PageLabelPrefix("A-").PageLabel(1)
	pic4(fonts, p5, 'a')

	pic4(fonts, d.Pages.AddPage(pic), 'x')
	pic4(fonts, d.Pages.AddPage(pic), 'y')
	pic4(fonts, d.Pages.AddPage(pic), 'z')

	//------------
	fd, err := os.Create("/tmp/test.pdf")
	if err != nil {
		panic(err)
	}

	err = d.WriteTo(fd)
	if err != nil {
		panic(err)
	}
}

func pic1(fonts []gopdf.Font, p *gopdf.Page) {
	draw := p.Begin()
	defer draw.End()

	gray := gopdf.Gray{0.5}
	red := gopdf.RGB{1, 0, 0}

	draw.SetLineWidth(3)
	draw.SetStrokeColor(gray)
	draw.SetFillColor(red)

	px := gopdf.NewPath().
		MoveTo(20, 20).
		LineTo(300, 100).
		LineTo(300, 20)
	draw.Stroke(px)

	draw.Fill(gopdf.NewPath().
		MoveTo(25, 20).
		LineTo(290, 95).
		LineTo(290, 20))

	draw.SetFillColor(gopdf.Gray{0.333})
	t := draw.Text()
	t.SetFont(fonts[0], 12)
	t.Move(40, 10)
	//t.RenderMode(gopdf.TextRenderStroke)
	t.Show("Hello, world")
	t.End()
}

func pic2(fonts []gopdf.Font, p *gopdf.Page) {
	draw := p.Begin()
	defer draw.End()

	draw.SetFillColor(gopdf.Gray{0})
	t := draw.Text()
	t.SetFont(fonts[1], 12)
	t.Move(40, 50)
	t.Show("This is a TOC page")
	t.Move(40, 40)
	t.Show("here is another line")
	t.Move(40, 30)
	t.Show("and again")
	t.End()
}

func pic3(fonts []gopdf.Font, p *gopdf.Page) {
	draw := p.Begin()
	defer draw.End()

	draw.SetFillColor(gopdf.Gray{0})
	t := draw.Text()
	t.SetFont(fonts[0], 24)

	t.Move(40, 200)
	t.SetLineWidth(0.2)
	t.RenderMode(gopdf.TextRenderStroke)
	t.Show("Heading")

	t.RenderMode(gopdf.TextRenderFill)
	t.SetFont(fonts[1], 12)
	t.Move(0, -12)
	t.Show("1. Content line (1)")
	t.Move(0, -12)
	t.Show("2. another line (2)")
	t.Move(0, -12)
	t.Show("3. yet another line (3)")
	t.End()
}

func pic4(fonts []gopdf.Font, p *gopdf.Page, r rune) {
	draw := p.Begin()
	defer draw.End()

	f := fonts[0].(*ttf.TrueTypeFont)
	face := f.Scale(fixed.I(200))
	cm := face.Metrics(r)

	draw.SetFillColor(gopdf.Gray{0.9})
	draw.SetLineWidth(0.5)

	x0 := 50 + fixedfloat(cm.BBox.Min.X)
	y0 := 50 + fixedfloat(cm.BBox.Min.Y)
	x1 := 50 + fixedfloat(cm.BBox.Max.X)
	y1 := 50 + fixedfloat(cm.BBox.Max.Y)

	bbox := gopdf.NewPath().
		MoveTo(x0, y0).
		LineTo(x0, y1).
		LineTo(x1, y1).
		LineTo(x1, y0).
		ClosePath()
	draw.Fill(bbox)

	draw.SetStrokeColor(gopdf.Gray{0})
	draw.SetFillColor(gopdf.Gray{1})

	t := draw.Text()
	t.SetFont(fonts[0], 200)

	t.RenderMode(gopdf.TextRenderStroke | gopdf.TextRenderFill)
	t.Move(50, 50)
	t.Show(string(r))

	baseline := gopdf.NewPath().
		MoveTo(50, 50).
		LineTo(50+fixedfloat(cm.Advance), 50)
	draw.Stroke(baseline)

}

func fixedfloat(f fixed.Int26_6) float64 {
	return float64(f) / 64.0
}
