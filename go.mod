module bitbucket.org/dkolbly/gopdf

go 1.14

require (
	dmitri.shuralyov.com/font/woff2 v0.0.0-20180220214647-957792cbbdab // indirect
	github.com/ConradIrwin/font v0.0.0-20190603172541-e12dbea4cf12
	github.com/dsnet/compress v0.0.1 // indirect
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0
	github.com/jung-kurt/gofpdf v1.16.2
	golang.org/x/image v0.0.0-20200119044424-58c23975cae1
)
