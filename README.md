# PDF Generator

This project is intended to be a fairly low-level driver for writing
PDF documents.  It arose because I was frustrated with the high level
abstractions and non-idiomatic Go style present in several other PDF
generator libraries.  My target use case is not document management,
but rather illustration, where the requirements are somewhat
different.

This is in early phases.

