package gopdf

import (
//	"bufio"
//	"fmt"
//	"io"
)

// ISO 32000 aka PDF 1.7

type Document struct {
	objects  []*object
	compress bool
	trailer  map[string]Datum
	info     *object
	root     *object
	Pages    *Pages
	fonts    map[int]*LoadedFont
}

// these are what PDF 1.7 calls "indirect objects"
type object struct {
	id     uint   // if free, link to next free object, else our own id
	gen    uint16 // generation number
	offset int    // offset into the file (-1 = free)
	item   Datum
}

func (o *object) ref() objref {
	return objref{
		id:  o.id,
		gen: o.gen,
	}
}

// New creates a new PDF document
func New() *Document {
	d := &Document{
		compress: true,
		trailer:  make(map[string]Datum),
		objects: []*object{
			&object{
				offset: -1,    // object 0 is always free
				gen:    65535, // and has generation 0xffff
			},
		},
	}

	// initialize the document structure

	// set up the /Root, which is the only required
	// object in the trailer besides the /Size
	d.root = d.alloc()
	cat := &Catalog{
		Dict: NewDict(Name("Catalog")),
	}
	d.root.item = cat
	d.trailer["Root"] = d.root.ref()

	// set up the /Pages root node, which is the only
	// required entry in the catalog
	d.Pages = d.newPages()
	cat.contents["Pages"] = d.Pages.ref()

	return d
}

func (d *Document) alloc() *object {
	o := &object{
		id: uint(len(d.objects)),
	}
	d.objects = append(d.objects, o)
	return o
}

func (d *Document) store(x Datum) *object {
	o := d.alloc()
	o.item = x
	return o
}

// Store arranges for the given object to be a first class object in
// the PDF and return an indirect reference to it
func (d *Document) Store(x Datum) Datum {
	return d.store(x).ref()
}
