package gopdf

import (
	"fmt"
)

type ColorSpace int

const (
	DeviceGray = ColorSpace(iota)
	DeviceRGB
	DeviceCMYK
)

type Color interface {
	Space() ColorSpace
	String() string
}

type Gray struct {
	G float64
}

func (c Gray) Space() ColorSpace {
	return DeviceGray
}

func (c Gray) String() string {
	return fmt.Sprintf("%.3g", c.G)
}

type RGB struct {
	R float64
	G float64
	B float64
}

func (c RGB) Space() ColorSpace {
	return DeviceRGB
}

func (c RGB) String() string {
	return fmt.Sprintf("%.3g %.3g %.3g", c.R, c.G, c.B)
}

type CMYK struct {
	C float64
	M float64
	Y float64
	K float64
}

func (c CMYK) Space() ColorSpace {
	return DeviceCMYK
}

func (c CMYK) String() string {
	return fmt.Sprintf("%.3g %.3g %.3g %.3g", c.C, c.M, c.Y, c.K)
}
