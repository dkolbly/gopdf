package gopdf

// c.f. 7.7.3 Page Tree
type Pages struct {
	Dict
	owner *Document
	self  *object
	kids  []PageNode
}

type PageNode interface {
	count() int
	ref() objref
	walk(pageVisitor)
}

type pageVisitor interface {
	group(*Pages)
	leaf(*Page)
}

func (p *Pages) walk(v pageVisitor) {
	v.group(p)
	for _, k := range p.kids {
		k.walk(v)
	}
}

func (p *Pages) ref() objref {
	return p.self.ref()
}

func (d *Document) newPages() *Pages {
	obj := d.alloc()
	p := &Pages{
		Dict:  NewDict(Name("Pages")),
		owner: d,
		self:  obj,
	}
	obj.item = p
	return p
}

func (p *Pages) count() int {
	n := 0
	for _, k := range p.kids {
		n += k.count()
	}
	return n
}

func (p *Pages) flush() {
	kids := make(Array, len(p.kids))
	for i, k := range p.kids {
		kids[i] = k.ref()
	}
	p.contents["Kids"] = kids
	p.contents["Count"] = Integer(p.count())
}
