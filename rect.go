package gopdf

import (
	"fmt"
	"io"
)

// c.f. 7.9.5
type Rect struct {
	LL, UR Point
}

type Point struct {
	X, Y float64
}

func (r Rect) WriteTo(dst io.Writer) error {
	fmt.Fprintf(dst, "[%g %g %g %g]",
		r.LL.X,
		r.LL.Y,
		r.UR.X,
		r.UR.Y,
	)
	return nil
}

func NewRect(x, y, w, h float64) Rect {
	return Rect{
		LL: Point{
			X: x,
			Y: y,
		},
		UR: Point{
			X: x + w,
			Y: y + h,
		},
	}
}
