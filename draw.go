package gopdf

import (
	"bytes"
	"fmt"
	"math"
)

type Drawing struct {
	p   *Page
	con *lineBreaker //*Stream
}

func (d *Drawing) format(form string, args ...interface{}) {
	d.con.format(form, args...)
}

func (p *Page) Begin() *Drawing {
	d := &Drawing{
		p:   p,
		con: newLineBreaker(p.Content()),
	}
	fmt.Fprintf(d.con, "q")
	return d
}

func (d *Drawing) End() {
	d.con.space(72)
	fmt.Fprintf(d.con, "Q\n")
}

type Path struct {
	buf    bytes.Buffer
	in     bool
	cx, cy float64
}

func NewPath() *Path {
	return &Path{}
}

func (p *Path) format(form string, args ...interface{}) {
	format(&p.buf, form, args...)
}

func (p *Path) Rect(x, y, w, h float64) *Path {
	p.format(" % % % % re", x, y, w, h)
	p.in = false
	return p
}

func (p *Path) MoveTo(x, y float64) *Path {
	p.format(" % % m", x, y)
	p.cx, p.cy, p.in = x, y, true
	return p
}

func (p *Path) LineTo(x, y float64) *Path {
	if !p.in {
		panic("LineTo() without a current point")
	}
	p.format(" % % l", x, y)
	p.cx, p.cy = x, y
	return p
}

// CurveTo() draws a bezier curve using control points (x0, y0) and (x1, y1)
// to (x, y)
func (p *Path) CurveTo(x0, y0, x1, y1, x, y float64) *Path {
	if !p.in {
		panic("CurveTo() without a current point")
	}
	p.format(" % % % % % % c", x0, y0, x1, y1, x, y)
	p.cx, p.cy = x, y
	return p
}

func (p *Path) Curve(x0, y0, x1, y1, x, y float64) *Path {
	if !p.in {
		panic("Curve() without a current point")
	}
	p.format(" % % % % % % c",
		x0+p.cx, y0+p.cy,
		x1+p.cx, y1+p.cy,
		x+p.cx, y+p.cy)
	p.cx, p.cy = p.cx+x, p.cy+y
	return p
}

func (p *Path) Line(dx, dy float64) *Path {
	if !p.in {
		panic("Line() without a current point")
	}
	p.cx, p.cy = p.cx+dx, p.cy+dy
	p.format(" % % l", p.cx, p.cy)
	return p
}

func (p *Path) ClosePath() *Path {
	fmt.Fprintf(&p.buf, " h")
	p.in = false
	return p
}

func (d *Drawing) Stroke(p *Path) {
	d.con.Write(p.buf.Bytes())
	fmt.Fprintf(d.con, " S")
}

func (d *Drawing) Fill(p *Path) {
	d.con.Write(p.buf.Bytes())
	fmt.Fprintf(d.con, " f")
}

func (d *Drawing) FillStroke(p *Path) {
	d.con.Write(p.buf.Bytes())
	fmt.Fprintf(d.con, " b")
}

func (d *Drawing) SetLineWidth(w float64) {
	d.format(" % w", w)
}

func (d *Drawing) SetLineCap(mode int) {
	fmt.Fprintf(d.con, " %d J", mode)
}

func (d *Drawing) SetLineJoin(mode int) {
	fmt.Fprintf(d.con, " %d j", mode)
}

func (d *Drawing) SetLineDash(dash []float64, phase float64) {
	fmt.Fprintf(d.con, " %.6g %.6g d", dash, phase)
}

func (d *Drawing) SetStrokeColor(c Color) {
	d.con.space(72)
	switch c.Space() {
	case DeviceGray:
		fmt.Fprintf(d.con, "%s G", c)
	case DeviceRGB:
		fmt.Fprintf(d.con, "%s RG", c)
	case DeviceCMYK:
		fmt.Fprintf(d.con, "%s K", c)
	default:
		panic("unknown color space")
	}
}

func (d *Drawing) Comment(text string) {
	d.con.newline()
	fmt.Fprintf(d.con, "%% %s\n", text)
}

func (d *Drawing) SetFillColor(c Color) {
	d.con.space(72)
	switch c.Space() {
	case DeviceGray:
		fmt.Fprintf(d.con, "%s g", c)
	case DeviceRGB:
		fmt.Fprintf(d.con, "%s rg", c)
	case DeviceCMYK:
		fmt.Fprintf(d.con, "%s k", c)
	default:
		panic("unknown color space")
	}
}

func (d *Drawing) PushState() {
	d.con.space(72)
	fmt.Fprintf(d.con, "q")
}

func (d *Drawing) PopState() {
	d.con.space(72)
	fmt.Fprintf(d.con, "Q")
}

func (d *Drawing) TransformTranslate(dx, dy float64) {
	d.con.space(72)
	// there's no shorthand operator for translate??
	d.format("1 0 0 1 % % cm", dx, dy)
}

func (d *Drawing) TransformScale(sx, sy float64) {
	d.con.space(72)
	// there's no shorthand operator for scale??
	d.format("% 0 0 % 0 0 cm", sx, sy)
}

// Rotation in degrees
func (d *Drawing) TransformRotate(q float64) {
	d.con.space(72)
	// there's no shorthand operator for rotate??
	s, c := math.Sincos(q * math.Pi / 180)
	d.format("% % % % 0 0 cm", c, s, -s, c)
}

// shorthand for scaling by x=1 y=-1
func (d *Drawing) Flip() {
	d.con.space(72)
	fmt.Fprintf(d.con, "1 0 0 -1 0 0 cm")
}
