package gopdf

var USLetter = Rect{
	LL: Point{0, 0},
	UR: Point{612, 792},
}

var Paper = map[string]Rect{
	"US Letter": USLetter,
}
