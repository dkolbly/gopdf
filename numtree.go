package gopdf

// c.f. 7.9.7 Number Trees

// currently, we only use number tree to map page indices to page labels
// (not clear why this information is not stored in Page objects :/)

func (d *Document) generateNumTree() Dict {
	lw := &labelwalker{}
	d.Pages.walk(lw)

	// TODO if this is too big, blow it out into a tree instead

	plt := NewDict("")

	// initial conditions
	if len(lw.labels) > 0 {
		var a Array
		s := lw.labels[0].Style
		for i, l := range lw.labels {
			if i == 0 || l.mask != 0 {
				d0 := NewDict("")
				if l.mask&0x4 != 0 {
					s = l.Style
				}
				d0.contents["S"] = s.tag()
				if l.mask&0x1 != 0 {
					d0.contents["St"] = Integer(l.Number)
				}
				if l.mask&0x2 != 0 {
					d0.contents["P"] = Str(l.Prefix)
				}
				a = append(a, Integer(i))
				a = append(a, d0)
			}
		}
		plt.contents["Nums"] = a
	}
	return plt
}

type labelwalker struct {
	labels []PageLabel
}

func (l *labelwalker) group(pg *Pages) {
}

func (l *labelwalker) leaf(p *Page) {
	l.labels = append(l.labels, p.label)
}
