package gopdf

import (
	"bufio"
	"fmt"
	"io"
)

// some datum objects implement flush(), which helps them finishing setting up
// their representation before being written out
type flusher interface {
	flush()
}

type outputFile struct {
	dst *bufio.Writer
	at  int
}

func (o *outputFile) Tell() int {
	return o.at
}

func (o *outputFile) Flush() error {
	return o.dst.Flush()
}

func (o *outputFile) Write(data []byte) (int, error) {
	n, err := o.dst.Write(data)
	o.at += n
	return n, err
}

// finalize the document
func (doc *Document) flush() {

	// store the page label number tree in the catalog
	pnto := doc.alloc()
	pnto.item = doc.generateNumTree()
	doc.root.item.(*Catalog).contents["PageLabels"] = pnto.ref()

	doc.trailer["Size"] = Integer(len(doc.objects))
}

// WriteTo writes a PDF document to a stream

func (doc *Document) WriteTo(d io.Writer) error {

	doc.flush()

	dst := &outputFile{
		dst: bufio.NewWriter(d),
	}

	// write the file header
	// 7.5.2 - File Header
	fmt.Fprintf(dst, "%%PDF-1.7\n")

	if doc.compress {
		// we will likely contain binary data; follow the header with
		// a comment line that contains some binary data.  Choose
		// binary data representing a UTF character.  Since Go is natively UTF,
		// we can just include it in a string
		fmt.Fprintf(dst, "%% Go❤PDF\n")
	}
	// write the body
	// write the indirect objects
	for _, o := range doc.objects {
		if o.offset < 0 {
			// it's a free object
			continue
		}
		o.offset = dst.Tell()
		fmt.Fprintf(dst, "%d %d obj\n", o.id, o.gen)
		if f, ok := o.item.(flusher); ok {
			f.flush()
		}
		o.item.WriteTo(dst)
		fmt.Fprintf(dst, "\nendobj\n")
	}

	// write the cross-reference section
	// 7.5.4  Cross-Reference Table

	last := dst.Tell()
	fmt.Fprintf(dst, "xref\n")
	fmt.Fprintf(dst, "%d %d\n", 0, len(doc.objects))
	for _, o := range doc.objects {
		if o.offset < 0 {
			fmt.Fprintf(dst, "%010d %05d f \n", o.id, o.gen)
		} else {
			fmt.Fprintf(dst, "%010d %05d n \n", o.offset, o.gen)
		}
	}

	// 7.5.5 File Trailer
	fmt.Fprintf(dst, "trailer\n")
	// use datum dict writer for deterministic ordering;
	// it's a pdf dict anyway
	Dict{doc.trailer}.WriteTo(dst)
	fmt.Fprintf(dst, "startxref\n")
	fmt.Fprintf(dst, "%d\n", last)
	fmt.Fprintf(dst, "%%%%EOF\n")

	return dst.Flush()
}
