package gopdf

import (
	"bytes"
	"fmt"
)

// Fonts are managed outside of the document itself, so that the application
// can load them up separately.  When they get attached to a document using
// SetFont(), the underlying data gets hooked in to the document

type Font interface {
	MakeFontDict(doc *Document, subset map[rune]bool) (Dict, error)
}

func ASCII85Encode(src []byte) []byte {
	var out bytes.Buffer

	var tmp [5]byte
	tmpout := func(b int) {
		tmp[4] = byte(33 + (b % 85))
		b /= 85
		tmp[3] = byte(33 + (b % 85))
		b /= 85
		tmp[2] = byte(33 + (b % 85))
		b /= 85
		tmp[1] = byte(33 + (b % 85))
		b /= 85
		tmp[0] = byte(33 + b)
	}

	brk := 0
	for len(src) >= 4 {
		chunk := int(src[0]) << 24
		chunk += int(src[1]) << 16
		chunk += int(src[2]) << 8
		chunk += int(src[3])
		if out.Len() > brk {
			out.WriteByte('\n')
			brk += 72
		}
		if chunk == 0 {
			// special case
			out.WriteByte('z')
		} else {
			tmpout(chunk)
			out.Write(tmp[:])
		}
		src = src[4:]
	}
	if len(src) > 0 {
		if out.Len() > brk {
			out.WriteByte('\n')
			brk += 72
		}
		chunk := int(src[0]) << 24
		if len(src) > 1 {
			chunk += int(src[1]) << 16
		}
		if len(src) > 2 {
			chunk += int(src[2]) << 8
		}
		tmpout(chunk)
		out.Write(tmp[:len(src)+1])
	}
	out.Write([]byte{'~', '>'})

	return out.Bytes()
}

type LoadedFont struct {
	key string
	obj *object
	fnt Font
}

func (doc *Document) resolveFont(f Font) *LoadedFont {
	if doc.fonts == nil {
		doc.fonts = make(map[int]*LoadedFont)
	}

	for _, lf := range doc.fonts {
		if lf.fnt == f {
			return lf
		}
	}

	k := 10 + len(doc.fonts)
	lf := &LoadedFont{
		obj: doc.alloc(),
		fnt: f,
		key: fmt.Sprintf("F%d", k),
	}
	fd, err := f.MakeFontDict(doc, nil)
	if err != nil {
		// better error handling?
		panic(err)
	}
	lf.obj.item = fd
	doc.fonts[k] = lf
	return lf
}

func (p *Page) usesFont(lf *LoadedFont) {
	p.usesFonts[lf] = true
}
