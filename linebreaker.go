package gopdf

import (
	"bytes"
	"io"
	"math"
	"strconv"
)

type lineBreaker struct {
	col  int
	next io.Writer
}

func newLineBreaker(dst io.Writer) *lineBreaker {
	return &lineBreaker{
		next: dst,
	}
}

// force a newline
func (lb *lineBreaker) newline() {
	if lb.col == 0 {
		return
	}
	lb.next.Write([]byte{'\n'})
	lb.col = 0
}

func (lb *lineBreaker) space(width int) {
	if lb.col == 0 {
		// already have a space
		return
	}

	if lb.col >= width {
		lb.next.Write([]byte{'\n'})
		lb.col = 0
	} else {
		lb.next.Write([]byte{' '})
		lb.col++
	}
}

func (lb *lineBreaker) Write(data []byte) (int, error) {
	k := bytes.LastIndexByte(data, '\n')
	if k < 0 {
		lb.col += len(data)
	} else {
		lb.col = len(data[k+1:])
	}
	return lb.next.Write(data)
}

func (lb *lineBreaker) format(form string, args ...interface{}) {
	var tmp bytes.Buffer
	format(&tmp, form, args...)
	buf := tmp.Bytes()
	lb.next.Write(buf)
	lb.col += len(buf) // should not be any newlines in there
}

// PDF doesn't handle exponential notation, and Go does not
// have an variation of %f that does not fix the number of digits
// after the decimal point, so we have to implement it ourself
func float2str(f float64) string {
	_, exp := math.Frexp(f)
	if exp < -20 {
		return "0" // less 1/1000000 ; i.e., approximately zero
	} else if exp < -4 {
		return strconv.FormatFloat(f, 'g', 6, 64)
	} else {
		tmp := strconv.FormatFloat(f, 'f', 9, 64)
		// throw away trailing zeros
		for tmp[len(tmp)-1] == '0' || (len(tmp) > 7 && tmp[len(tmp)-1] != '.') {
			tmp = tmp[:len(tmp)-1]
		}
		if tmp[len(tmp)-1] == '.' {
			return tmp[:len(tmp)-1]
		}
		return tmp
	}
}

func format(dest *bytes.Buffer, form string, args ...interface{}) {
	for _, ch := range form {
		if ch == '%' {
			switch arg := args[0].(type) {
			case string:
				dest.WriteString(arg)
			case float64:
				dest.WriteString(float2str(arg))
			case int:
				dest.WriteString(strconv.FormatInt(int64(arg), 10))
			}
			args = args[1:]
		} else {
			dest.WriteRune(ch)
		}
	}
}
