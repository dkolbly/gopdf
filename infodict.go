package gopdf

// c.f. 14.3.3. Document Information Dictionary

func (d *Document) getinfo() Dict {
	if d.info == nil {
		d.info = d.alloc()
		d.info.item = NewDict("")
	}
	return d.info.item.(Dict)
}

func (d *Document) SetInfo(key string, value string) {
	d.getinfo().contents[key] = Str(value)
}
