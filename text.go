package gopdf

import (
	"fmt"
)

func (d *Drawing) Text() Text {
	fmt.Fprintf(d.con, "\nBT")
	return Text{d}
}

// shorthand translation
func (t Text) Flip() {
	t.con.space(72)
	fmt.Fprintf(t.con, "1 0 0 -1 0 0 Tm")
}

// Move moves relative to the last place we moved to
func (t Text) Move(x, y float64) {
	t.con.space(72)
	fmt.Fprintf(t.con, "%g %g Td", x, y)
}

type Text struct {
	*Drawing
}

func (t Text) SetFont(f Font, size float64) {
	doc := t.p.owner
	lf := doc.resolveFont(f)
	t.p.usesFont(lf)
	t.con.space(72)
	fmt.Fprintf(t.con, "/%s %g Tf", lf.key, size)
}

func (t Text) End() {
	fmt.Fprintf(t.con, " ET\n")
}

func (t Text) Show(text string) {
	t.con.space(72)
	Str(text).WriteTo(t.con)
	fmt.Fprintf(t.con, " Tj")
}

// c.f. 9.3.6 Text Rendering Mode
type TextRenderMode uint

const (
	TextRenderFill = TextRenderMode(1 << iota)
	TextRenderStroke
	TextRenderClip
)

var pdfTextModes = map[TextRenderMode]int{
	TextRenderFill:                    0,
	TextRenderStroke:                  1,
	TextRenderFill | TextRenderStroke: 2,
	0:                                 3,
	TextRenderFill | TextRenderClip:   4,
	TextRenderStroke | TextRenderClip: 5,
	TextRenderFill | TextRenderStroke | TextRenderClip: 6,
	TextRenderClip: 7,
}

func (t Text) SetRenderMode(mode TextRenderMode) {
	if mode, ok := pdfTextModes[mode]; ok {
		t.con.space(72)
		fmt.Fprintf(t.con, "%d Tr", mode)
	} else {
		panic("invalid text render mode")
	}
}
